package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"

	"github.com/joho/godotenv"
)

var a App

/* ---------------------------------- TESTS ---------------------------------- */

// Initializes the DB for tests
func TestMain(m *testing.M) {

	// Loads env variables from a file
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	a.Initialize(
		os.Getenv("APP_DB_USERNAME"),
		os.Getenv("APP_DB_PASSWORD"),
		os.Getenv("APP_DB_NAME"))

	ensureTableExists()
	code := m.Run()
	clearTable()
	os.Exit(code)

}

// Clears the table, then makes a GET request to make sure the table is actually empty
func TestEmptyTable(t *testing.T) {
	clearTable()

	req, _ := http.NewRequest("GET", "/products", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body != "[]" {
		t.Errorf("Expected an empty array. Got %s", body)
	}
}

// Checks the response when fetching a nonexistent product
// It assesses 2 things: The response code is 404, and it contains the message "Product not found"
func TestGetNonExistentProduct(t *testing.T) {
	clearTable()

	req, _ := http.NewRequest("GET", "/product/11", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusNotFound, response.Code)

	var m map[string]string
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["error"] != "Product not found" {
		t.Errorf("Expected the 'error' key of the response to be set to 'Product Not Found'. Got '%s'", m["error"])
	}
}

// Manually adds a product to the DB, then access the relevent endpoint to fetch it.
// We test that response has a status code of 201, and the object received is identical
func TestCreateProduct(t *testing.T) {
	clearTable()

	// Create a JSON manually, then create a post request that sends it
	var jsonStr = []byte(`{"name":"test product", "price": 11.22}`)
	req, _ := http.NewRequest("POST", "/product", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	response := executeRequest(req)
	checkResponseCode(t, http.StatusCreated, response.Code)

	// We use a map with interface values to store the response, for some reason. Probably to store errors?
	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	// If the name is incorrect
	if m["name"] != "test product" {
		t.Errorf("Expected product name to be 'test product'. Got '%v'", m["name"])
	}

	// If the price is incorrect
	if m["price"] != 11.22 {
		t.Errorf("Expected product price to be '11.22'. Got '%v'", m["price"])
	}

	// The id is compared to 1.0 because JSON unmarshaling converts numbers to floats, when the target is a map[string]interface{}
	if m["id"] != 1.0 {
		t.Errorf("Expected product ID to be '1', got '%v'", m["id"])
	}

}

// Adds a product to the table and tests that accessing the relevant endpoint results in a 200 HTTP response
func TestGetProduct(t *testing.T) {
	clearTable()
	addProducts(1)

	req, _ := http.NewRequest("GET", "/product/1", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

// Adds a product to the database, then uses the endpoint to update it.
// We test for status code 200 and that the response contains the updated JSON
func TestUpdateProduct(t *testing.T) {

	clearTable()
	addProducts(1)

	req, _ := http.NewRequest("GET", "/product/1", nil)
	response := executeRequest(req)
	var originalProduct map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &originalProduct)

	var jsonStr = []byte(`{"name":"test product - updated name", "price": 11.22}`)
	req, _ = http.NewRequest("PUT", "/product/1", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	response = executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	if m["id"] != originalProduct["id"] {
		t.Errorf("Expected the id to remain the same (%v). Got %v", originalProduct["id"], m["id"])
	}

	if m["name"] == originalProduct["name"] {
		t.Errorf("Expected the name to change from '%v' to '%v'. Got %v", originalProduct["name"], m["name"], m["name"])
	}

	if m["price"] == originalProduct["price"] {
		t.Errorf("Expected the price to change from '%v' to '%v'. Got %v", originalProduct["price"], m["price"], m["price"])
	}
}

// Add a product, test if it exists, then we delete using the endpoint. Finally, we check again
// to make sure that it was deleted
func TestDeleteProduct(t *testing.T) {
	clearTable()
	addProducts(1)

	req, _ := http.NewRequest("GET", "/product/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("DELETE", "/product/1", nil)
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("GET", "/product/1", nil)
	response = executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)
}

/* ---------------------------------- UTILITIES ---------------------------------- */

const tableCreationQuery = `create table if not exists "goSchema".products (
	id serial,
	name text not null,
	price numeric(10,2) not null default 0.00,
	constraint products_pkey primary key (id)
);`

// Verifies that a table exists
func ensureTableExists() {
	if _, err := a.DB.Exec(tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

// Deletes everything from the existing table
func clearTable() {
	a.DB.Exec(`DELETE FROM "goSchema".products`)
	_, err := a.DB.Exec(`ALTER SEQUENCE "goSchema".products_id_seq RESTART WITH 1`)
	if err != nil {
		fmt.Println("There was an issue altering the sequence:", err)
	}
}

// Executes a request using the application's router and returns the response
func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

// Compares two response codes to see if they are the same
func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("expected response code %d. Got %d\n", expected, actual)
	}
}

// Adds one or more records into the table for testing purposes
func addProducts(count int) {
	if count < 1 {
		count = 1
	}

	for i := 0; i < count; i++ {
		_, err := a.DB.Exec(`INSERT INTO "goSchema".products(name, price) VALUES($1,$2)`, "Product "+strconv.Itoa(i), (i+1.0)*10)
		if err != nil {
			log.Println("Issue in addProducts:", err)
		}
	}
}

func TestTemp(t *testing.T) {

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	a.Initialize(
		os.Getenv("APP_DB_USERNAME"),
		os.Getenv("APP_DB_PASSWORD"),
		os.Getenv("APP_DB_NAME"))

	clearTable()
	addProducts(1)

	req, _ := http.NewRequest("GET", "/product/1", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}
