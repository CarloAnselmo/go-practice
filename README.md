# Go Practice

You're gonna need to create your own .env file if you want this code to work. Inside of it, enter your own credentials:

APP_DB_USERNAME=

APP_DB_PASSWORD=

APP_DB_NAME=

Additionally, the schema that's been hardcoded into the program is titled "mySchema", so you'll likely need to create it for the program to run on your setup.
